//
//  ASAppDelegate.m
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
@implementation ASAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    
#warning this is for testing, delete this
   //[ASUtility savePropertyData:nil withKey:keyforAPIKEY];
    
    NSString *apikey = @"";
    
    apikey = [ASUtility propertyDataWithKey:keyforAPIKEY];
    
    if(apikey.length>0 && apikey !=nil){
     [self showMainView];   
    }
    else{
    ASLoginViewController*loginScreen  = [[ASLoginViewController alloc] initWithNibName:@"ASLoginViewController" bundle:nil];
    loginScreen.delegate = self;
    self.activeViewController = loginScreen;
    [self.MainView addSubview:self.activeViewController.view];
    }
    
}

-(void)LoginSuccess{
    [self.activeViewController.view removeFromSuperview];
    [self showMainView];
}

-(void)showMainView{
    self.mainViewController = [[ASMainViewController alloc] initWithNibName:@"ASMainViewController" bundle:nil];
    self.activeViewController = self.mainViewController;
    [self.MainView addSubview:self.activeViewController.view];
}

@end
