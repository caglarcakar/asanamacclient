//
//  ASMainViewController.m
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASMainViewController.h"

@interface ASMainViewController ()

@end

@implementation ASMainViewController{
   
}
@synthesize workspaces,projects,tasks,selectedProject,selectedTask,selectedWorkSpace;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
-(void)awakeFromNib{
    ASUser *user = [ASUser new];
    user = [ASUserManager loadUser];
    
    
    
    [AsanaManager getWorkSpaces:^(BOOL requestSuccess, NSString *message, NSMutableArray *remoteWorkspaces) {
        
        if(requestSuccess) {
            NSLog(@"got the workspaces");
            self.workspaces = [NSMutableArray arrayWithArray:remoteWorkspaces];
            self.workspaceController.workspacesArray = [NSMutableArray arrayWithArray:self.workspaces];
            self.workspaceController.delegate = self;
            [self.workspaceTable reloadData];
            NSLog(@"ws count= %li",(unsigned long)[self.workspaces count]);
        }
        else{
            [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
        }
        
    } failure:^(NSString *message) {
        NSLog(@"%@",message);
        [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
    }];
    
    
    NSLog(@"user name = %@",user.name);
}

#pragma mark Delegate Methods

-(void)workspaceSelected:(ASWorkspace *)aWorkspace{
    NSLog(@"name : %@",aWorkspace.name);
    self.selectedWorkSpace = aWorkspace;
    [self loadProjects];
}

-(void)projectSelected:(ASProject *)aProject{
    NSLog(@"name : %@",aProject.name);
    self.selectedProject = aProject;
    [self loadTasks];
}
-(void)taskSelected:(ASTask *)aTask{
    [self getTaskDetail:aTask];
}

-(void)loadProjects{
    [AsanaManager getProjects:self.selectedWorkSpace.ID success:^(BOOL requestSuccess, NSString *message, NSMutableArray *remoteprojects) {
        
        if(requestSuccess) {
            NSLog(@"got the projects");
            self.projects = [NSMutableArray arrayWithArray:remoteprojects];
            self.projectController.projectsArray = [NSMutableArray arrayWithArray:self.projects];
            self.projectController.delegate = self;
            [self.projectTable reloadData];
            NSLog(@"prj count= %li",(unsigned long)[self.projects count]);
        }
        else{
            [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
        }

        
    } failure:^(NSString *message) {
        [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
    }];
}

-(void)loadTasks{
    [AsanaManager getTasks:self.selectedProject.ID success:^(BOOL requestSuccess, NSString *message, NSMutableArray *remotetasks) {
        
        if(requestSuccess) {
            NSLog(@"got the tasks");
            self.tasks = [NSMutableArray arrayWithArray:remotetasks];
            self.taskController.tasksArray = [NSMutableArray arrayWithArray:self.tasks];
            self.taskController.delegate = self;
            [self.TaskTable reloadData];
            NSLog(@"task count= %li",(unsigned long)[self.tasks count]);
        }
        else{
            [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
        }

        
    } failure:^(NSString *message) {
        [ASUtility showAlertWithText:message withDetailText:nil inWindow:[self.view window]];
    }];
}

-(void)getTaskDetail:(ASTask *)aTask{
   [AsanaManager getTaskDetail:aTask.ID success:^(BOOL requestSuccess, NSString *message, ASTask *remoteSelectedTask) {
       
       NSLog(@"task detail is taken %@",remoteSelectedTask.description);
       self.selectedTask = [ASTask new];
       self.selectedTask = remoteSelectedTask;
       [self.taskName setStringValue:self.selectedTask.name];
       
   } failure:^(NSString *message) {
       
   }];
}


- (IBAction)saveTaskDetail:(id)sender {
    
    self.selectedTask.name = [self.taskName stringValue];
    
    
    [AsanaManager saveTaskDetail:self.selectedTask success:^(BOOL requestSuccess, NSString *message) {
        NSLog(@"KAYIT YAPILDI");
    } failure:^(NSString *message) {
        NSLog(@"HATA");
    }];
}


@end
