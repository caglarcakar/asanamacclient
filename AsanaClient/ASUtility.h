//
//  ASUtility.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DijitalGarajRequestObject.h"

@interface ASUtility : NSObject

+(void)showAlertWithText:(NSString *)message withDetailText:(NSString *)detailText inWindow:(NSWindow *)window;
+(void)savePropertyData:(NSString *)data withKey:(NSString *)key;
+(void)saveBoolPropertyData:(BOOL)data withKey:(NSString *)key;
+(NSString *)propertyDataWithKey:(NSString *)key;
+(BOOL)boolPropertyDataWithKey:(NSString *)key;
+(void)deActivateButton:(NSButton *)button;
+(void)activateButton:(NSButton *)button;
@end
