//
//  ASAppDelegate.h
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ASLoginViewController.h"
#import "ASMainViewController.h"
@interface ASAppDelegate : NSObject <NSApplicationDelegate,loginDelegate>

@property (assign) IBOutlet NSWindow *window;
@property(nonatomic,retain) IBOutlet NSViewController *activeViewController;
@property (weak) IBOutlet NSView *MainView;

@property(retain,nonatomic)ASMainViewController *mainViewController;

@end
