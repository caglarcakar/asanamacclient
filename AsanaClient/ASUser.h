//
//  ASUser.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASUser : NSObject

@property(nonatomic)NSString *ID;
@property(nonatomic)NSString *name;
@property(nonatomic)NSString *email;
@property(nonatomic)NSString *image;
@property(nonatomic)NSString *apiKey;

@end
