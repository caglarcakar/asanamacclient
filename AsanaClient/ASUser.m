//
//  ASUser.m
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASUser.h"

@implementation ASUser

- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
	[encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.ID forKey:@"ID"];
    [encoder encodeObject:self.apiKey forKey:@"apikey"];
    //[encoder encodeObject:self.name forKey:@"name"];
    //[encoder encodeObject:self.name forKey:@"name"];
    
}
- (id)initWithCoder:(NSCoder *)decoder
{
        self = [super init];
        if( self != nil )
        {
            //decode properties, other class vars
            self.name = [decoder decodeObjectForKey:@"name"];
            self.ID = [decoder decodeObjectForKey:@"ID"];
            self.apiKey = [decoder decodeObjectForKey:@"apikey"];
           // self.name = [decoder decodeObjectForKey:@"name"];
        }
    return self;
}

@end
