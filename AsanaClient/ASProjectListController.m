//
//  ASProjectListController.m
//  AsanaClient
//
//  Created by caglar cakar on 8/3/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASProjectListController.h"
#import "ASProjectCellViewController.h"
@implementation ASProjectListController

-(id)initWithArray:(NSMutableArray *)anArray{
    
    self = [super init];
    if(self){
        self.projectsArray = [[NSMutableArray alloc] initWithArray:anArray];
    }
    return self;
}


#pragma mark tableview Delegate

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    
        return [self.projectsArray count];
    

}

-(NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    
    
    
        
        ASProjectCellViewController *cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];
    
    ASProject *aProject = [ASProject new];
    aProject = [self.projectsArray objectAtIndex:row];
    
    
        cellView.textField.stringValue = aProject.name;
        
        return cellView;
        
   
    
}

-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSLog(@"selection");
    
    ASProject*selectedProject = [ASProject new];
    
    selectedProject = [self.projectsArray objectAtIndex:[notification.object selectedRow]];
    
    if([self.delegate respondsToSelector:@selector(projectSelected:)]){
    [self.delegate projectSelected:selectedProject];
    }
    
}


@end
