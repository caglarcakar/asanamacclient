//
//  ASTaskListController.h
//  AsanaClient
//
//  Created by caglar cakar on 8/3/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol taskDelegate<NSObject>
@optional
-(void)taskSelected:(ASTask *)aTask;
@end
@interface ASTaskListController : NSObject<NSTableViewDataSource,NSTableViewDelegate>
-(id)initWithArray:(NSMutableArray *)anArray;
@property(copy)NSMutableArray *tasksArray;
@property(nonatomic,strong)id<taskDelegate>delegate;

@end
