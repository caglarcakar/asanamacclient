//
//  AsanaManager.m
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "AsanaManager.h"

@implementation AsanaManager


+(void)login:(void (^)(BOOL, NSString *, ASUser *))successfull failure:(void (^)(NSString *))failure{
    DijitalGarajRequestObject* loginRequest = [[DijitalGarajRequestObject alloc] initWithApi:@"/users/me/" andParameters:nil andTag:0 requsetMethod:@"GET" showProgress:NO ];
    
    [loginRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
        
        NSDictionary *data = [response objectForKey:@"data"];
        
        if(data && [data count]>0 && [[response objectForKey:@"data"] isKindOfClass:[NSDictionary class]]){
            ASUser *user = [ASUser new];
            
            user.ID = [data objectForKey:@"id"];
            user.name = [data objectForKey:@"name"];
            user.image = [data objectForKey:@"image_128x128"];
            user.email = [data objectForKey:@"email"];
            successfull(YES,@"",user);
            
        }
        else{
            //[ASUtility showAlertWithText:@"Something went wrong with your api key!" withDetailText:@"Please try again" inWindow:[self.view window]];
            
            failure(@"Something went wrong with your api key!");
        }
        
        // NSArray *workspaces = [response objectForKey:@"workspaces"];
        
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];
}

+(void)getWorkSpaces:(void (^)(BOOL, NSString *, NSMutableArray *))successfull failure:(void (^)(NSString *))failure{
    DijitalGarajRequestObject* loginRequest = [[DijitalGarajRequestObject alloc] initWithApi:@"/workspaces/" andParameters:nil andTag:0 requsetMethod:@"GET" showProgress:NO ];
    
    [loginRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
        NSMutableArray* workspaces = [NSMutableArray array];
        NSArray *data = [response objectForKey:@"data"];
        
        if(data && [data count]>0 && [[response objectForKey:@"data"] isKindOfClass:[NSArray class]]){
           
            ASWorkspace *header = [ASWorkspace new];
            header.name = @"WorkSpaces";
            header.isHeader = YES;
            
            [workspaces addObject:header];
            
            for(NSDictionary *aWorkspace in data){
                
                ASWorkspace *ws = [ASWorkspace new];
                ws.ID = [aWorkspace objectForKey:@"id"];
                ws.name = [aWorkspace objectForKey:@"name"];
                
                [workspaces addObject:ws];
            }
            
            successfull(YES,@"",workspaces);
        }
        else{
            failure(@"Something went wrong!");
        }
        
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];
}
+(void)getProjects:(NSString *)workspaceID success:(void (^)(BOOL, NSString *, NSMutableArray *))successfull failure:(void (^)(NSString *))failure{
    
    NSString *apiPath = [NSString stringWithFormat:@"/workspaces/%@/projects",workspaceID];
    
    DijitalGarajRequestObject* loginRequest = [[DijitalGarajRequestObject alloc] initWithApi:apiPath andParameters:nil andTag:0 requsetMethod:@"GET" showProgress:NO ];
    
    [loginRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
         NSMutableArray* projects = [NSMutableArray array];
        NSArray *data = [response objectForKey:@"data"];
        
        if(data && [data count]>0 && [[response objectForKey:@"data"] isKindOfClass:[NSArray class]]){
           
            
            for(NSDictionary *aProject in data){
                
                ASProject *prj = [ASProject new];
                prj.ID = [aProject objectForKey:@"id"];
                prj.name = [aProject objectForKey:@"name"];
                
                [projects addObject:prj];
            }
            
            NSLog(@"loaded projects : %li",[projects count]);
            
            successfull(YES,@"",projects);
            
        }
        else{
            failure(@"Something went wrong!");
        }
                
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];
}
+(void)getTasks:(NSString *)taskID success:(void (^)(BOOL, NSString *, NSMutableArray *))successfull failure:(void (^)(NSString *))failure{
    
     NSString *apiPath = [NSString stringWithFormat:@"/projects/%@/tasks?&assignee=me&opt_fields=name,completed,due_on",taskID];
    
    DijitalGarajRequestObject* loginRequest = [[DijitalGarajRequestObject alloc] initWithApi:apiPath andParameters:nil andTag:0 requsetMethod:@"GET" showProgress:NO ];
    
    [loginRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
        NSMutableArray* tasks = [NSMutableArray array];
        NSArray *data = [response objectForKey:@"data"];
        
        if(data && [data count]>0 && [[response objectForKey:@"data"] isKindOfClass:[NSArray class]]){
            
            
            for(NSDictionary *aTask in data){
                
                ASTask *tsk = [ASTask new];
                tsk.ID = [aTask objectForKey:@"id"];
                tsk.name = [aTask objectForKey:@"name"];
                tsk.isCompleted = [[aTask objectForKey:@"completed"] integerValue];
                tsk.dueOn = [aTask objectForKey:@"due_on"];
                
                if([self isTaskPriorityHeading:tsk.name]){
                    tsk.isPriortyHeading = YES;
                }
                else{
                    tsk.isPriortyHeading = NO;
                }
                
                [tasks addObject:tsk];
            }

            
            successfull(YES,@"",tasks);
            
        }
        else{
            failure(@"Something went wrong!");
        }
        
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];
}


+(BOOL)isTaskPriorityHeading:(NSString *)taskname{
    
    if([taskname length]==0){
        return NO;
    }
    
    NSString *lastchar = [taskname substringFromIndex:[taskname length] - 1];
    
    if([lastchar isEqualToString:@":"]){
        return YES;
    }
    return NO;
    
}

+(void)getTaskDetail:(NSString *)taskID success:(void (^)(BOOL, NSString *, ASTask *))successfull failure:(void (^)(NSString *))failure{
    NSString *apiPath = [NSString stringWithFormat:@"/tasks/%@",taskID];
    
    DijitalGarajRequestObject* loginRequest = [[DijitalGarajRequestObject alloc] initWithApi:apiPath andParameters:nil andTag:0 requsetMethod:@"GET" showProgress:NO ];
    
    [loginRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
        //ASTask* selectedTask = [NSMutableArray array];
        NSDictionary *data = [response objectForKey:@"data"];
        
        if(data && [data count]>0 && [[response objectForKey:@"data"] isKindOfClass:[NSDictionary class]]){
            
            
            
                
                ASTask *task = [ASTask new];
                task.ID = [data objectForKey:@"id"];
                task.name = [data objectForKey:@"name"];
                task.isCompleted = [[data objectForKey:@"completed"] integerValue];
                task.dueOn = [data objectForKey:@"due_on"];
               task.notes = [data objectForKey:@"notes"];
                task.followers = [data objectForKey:@"followers"];
                if([self isTaskPriorityHeading:task.name]){
                    task.isPriortyHeading = YES;
                }
                else{
                    task.isPriortyHeading = NO;
                }
                
                
                successfull(YES,@"",task);
            
        }
        else{
            failure(@"Something went wrong!");
        }
        
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];

}

+(void)saveTaskDetail:(ASTask *)aTask success:(void (^)(BOOL, NSString *))successfull failure:(void (^)(NSString *))failure{
    
    NSString *apiPath = [NSString stringWithFormat:@"/tasks/%@",aTask.ID];
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        //aTask.assignee_id,@"assignee",
                                        //@"6099284848027",@"followers[0]",
                                        aTask.name,@"name",
                                        aTask.notes,@"notes",
                                        //aTask.project.ID,@"projects[0]",
                                        //aTask.isCompleted,@"completed"
                                         nil];
    
    
    DijitalGarajRequestObject* saveTaskRequest = [[DijitalGarajRequestObject alloc] initWithApi:apiPath andParameters:parameters andTag:0 requsetMethod:@"PUT" showProgress:NO ];
    
    [saveTaskRequest startRequestWithSuccess:^(NSInteger tag, NSString *message, id responseObject) {
        
        NSDictionary * response = (NSDictionary *)responseObject;
        ASTask* selectedTask = [NSMutableArray array];
        NSDictionary *data = [response objectForKey:@"data"];
        
        successfull(YES,@"");
        
    } failure:^(NSInteger tag, NSString *message) {
        failure(@"ERROR!");
    }];
        
        
}

@end
