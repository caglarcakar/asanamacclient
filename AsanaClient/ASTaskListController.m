//
//  ASTaskListController.m
//  AsanaClient
//
//  Created by caglar cakar on 8/3/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASTaskListController.h"
#import "ASTaskCellViewController.h"

@implementation ASTaskListController

-(id)initWithArray:(NSMutableArray *)anArray{
    
    self = [super init];
    if(self){
        self.tasksArray = [[NSMutableArray alloc] initWithArray:anArray];
    }
    return self;
}


#pragma mark tableview Delegate

-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView{
    
    return [self.tasksArray count];
    
    
}

-(NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row{
    
    ASTaskCellViewController *cellView;
    
    ASTask *aTask = [ASTask new];
    aTask = [self.tasksArray objectAtIndex:row];
    
    if(aTask.isPriortyHeading){
        
      cellView = [tableView makeViewWithIdentifier:@"headingCell" owner:self];  
    }
    else{
      cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];  
    }
    
    //ASTaskCellViewController *cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];
    
    cellView.textField.stringValue = aTask.name;
    if(aTask.isCompleted){
    [cellView.completedCheck setState:NSOnState];
    }
    else{
    [cellView.completedCheck setState:NSOffState];
    }
    
    return cellView;

}

-(void)tableViewSelectionDidChange:(NSNotification *)notification{
    NSLog(@"selection");
    
    ASTask*selectedTask = [ASTask new];
    
    selectedTask = [self.tasksArray objectAtIndex:[notification.object selectedRow]];
    
    if([self.delegate respondsToSelector:@selector(taskSelected:)]){
        [self.delegate taskSelected:selectedTask];
    }
    
}




@end
