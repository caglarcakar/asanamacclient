//
//  DijitalGarajRequestObject.m
//  Dijital Garaj
//
//  Created by caglar cakar on 3/29/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "DijitalGarajRequestObject.h"
#import "AFJSONRequestOperation.h"
#import "AFHTTPClient.h"
#import "NSData+Base64.h"
#import "SBJson.h"

@implementation DijitalGarajRequestObject

@synthesize requestCustomUrl,parameters,api,tag,method,requestShowProgress,requestUseCustomURL;
-(id)initWithApi:(NSString *)anApi andParameters:(NSMutableDictionary *)theParameters andTag:(NSInteger )aTag requsetMethod:(NSString *)aMethod showProgress:(BOOL)progress {
    
    self = [super init];
    if (self) {
       
        self.api =anApi;
        self.parameters = [NSMutableDictionary dictionaryWithDictionary:theParameters];
        self.tag= aTag;
        self.method = aMethod;
       
        self.requestShowProgress = progress;
    }
    return self;
}

-(void)startRequestWithSuccess:(void (^)(NSInteger, NSString *, id))successfull failure:(void (^)(NSInteger, NSString *))failure{
    if(self.requestShowProgress){
        //[SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeGradient];
    }
        [self makeRequestWithSuccess:^(NSString *requestMessage, id requestResponsObject) {
            successfull(self.tag,requestMessage,requestResponsObject);
        } requestFailure:^(NSString *requestMessage) {
            failure(self.tag,requestMessage);
        }
        wait:^{
                                    
        }];
}


-(void)makeRequestWithSuccess:(void (^)(NSString *requestMessage, id requestResponsObject))requestSuccessfull
               requestFailure:(void (^)(NSString *requestMessage))failure  wait:(void (^)(void))waitForSession{
    @autoreleasepool {
        NSString *apiIndex;
        apiIndex = self.api;
       
        
        NSLog(@"REQUEST PARAMETERS = %@",self.parameters);
        
        
        NSString *path = [NSString stringWithFormat:@"%@",apiIndex];
        
        //NSString *path = [NSString stringWithFormat:@"/tasks/6118896120773?opt_fields=name,completed,due_on"];
        
        NSURL *url = [NSURL URLWithString:[kASANA_API_URL stringByAppendingString:path]];
        
        AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
        NSMutableURLRequest *request = [client requestWithMethod:self.method
                                                     path:nil
                                               parameters:self.parameters];
        //NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        
        NSString *credential = [NSString stringWithFormat:@"%@:", [ASUtility propertyDataWithKey:keyforAPIKEY]];
        NSData *credentialData = [credential dataUsingEncoding:NSUTF8StringEncoding];
        NSString *authValue = [NSString stringWithFormat:@"Basic %@", [credentialData base64EncodingWithLineLength:0]];
        [request setValue:authValue forHTTPHeaderField:@"Authorization"];

        AFJSONRequestOperation *operation =
        [AFJSONRequestOperation
         JSONRequestOperationWithRequest:request
         success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
         {
             NSLog(@"response = %@ request detail = %@",JSON,request.description);
            // NSDictionary *responseDict;
             
                 requestSuccessfull(@"ok",JSON);
             
         }
         failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
         {
             NSLog(@"Error : %@", request.description);
             failure(@"hata");
         }];
        
        [operation start];
    }
}



@end
