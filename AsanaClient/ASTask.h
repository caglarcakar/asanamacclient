//
//  ASTask.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASTask : NSObject
@property(copy)NSString* ID;
@property(copy)NSString* name;
@property NSInteger badgeValue;
@property BOOL isCompleted;
@property BOOL isPriortyHeading;
@property(copy)NSString* dueOn;
@property(copy)NSString* assignee_id;
@property(copy)NSString* assignee_name;
@property(copy)NSString* completed_at;
@property(copy)NSString* created_at;
@property(copy)NSMutableArray* followers;
@property(copy)NSString* modified_at;
@property(copy)NSString* notes;
@property(copy)ASProject *project;
@property(copy)NSMutableArray *tags;
@end


/*

 data =     {
 assignee =         {
 id = 861707249051;
 name = "kemal sahin";
 };
 "assignee_status" = inbox;
 completed = 1;
 "completed_at" = "2013-07-25T10:59:34.237Z";
 "created_at" = "2013-07-11T17:00:06.824Z";
 "due_on" = "<null>";
 followers =         (
 {
 id = 6099284848027;
 name = "Caglar Cakar";
 },
 {
 id = 861707249051;
 name = "kemal sahin";
 }
 );
 id = 6467140362915;
 "modified_at" = "2013-07-25T10:59:46.095Z";
 name = "odul id";
 notes = "";
 parent = "<null>";
 projects =         (
 {
 id = 6099563946506;
 name = "bilkazan iPhone App";
 }
 );
 tags =         (
 );
 workspace =         {
 id = 6099293616334;
 name = "dijitalgaraj.com";
 };
 };



*/