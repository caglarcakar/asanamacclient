//
//  ASUtility.m
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASUtility.h"
#define propertiesKey   @"AppProperties.plist"
@implementation ASUtility{
    DijitalGarajRequestObject *utilityRequest;
}

+(void)showAlertWithText:(NSString *)message withDetailText:(NSString *)detailText inWindow:(NSWindow *)window{
    
    NSString *alertMessage=@"";
    NSString *alertDetail=@"";
    
    if(message == nil || message.length==0 || !message){
        return;
    }
    
    if(detailText == nil || detailText.length ==0 || !detailText){
        alertDetail =@"";
    }
    else{
        alertDetail = detailText;
    }
    
    alertMessage = message;
    
    
    NSAlert *alert = [[NSAlert alloc] init] ;
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:alertMessage];
    [alert setInformativeText:alertDetail];
    [alert setAlertStyle:NSWarningAlertStyle];
    
    [alert beginSheetModalForWindow:window modalDelegate:self didEndSelector:nil contextInfo:nil];
}

+(void)savePropertyData:(NSString *)data withKey:(NSString *)key{
    
	NSString *path = [self getPath];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    
    if (fileExists) {
       
        NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        [prefs setValue: data forKey:key];
        [prefs writeToFile: path atomically: YES];
    }
    else {
       
        NSMutableDictionary* prefs = [NSMutableDictionary dictionary];
        [prefs setValue: data forKey:key];
        [prefs writeToFile: path atomically: YES];
        
    }
}

+(void)saveBoolPropertyData:(BOOL)data withKey:(NSString *)key{
    
   NSString *path = [self getPath];
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    [prefs setObject:[NSNumber numberWithBool:data] forKey:key];
    [prefs writeToFile: path atomically: YES];
}

+(NSString *)propertyDataWithKey:(NSString *)key{
    
    NSString *path = [self getPath];
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    NSString* data = (NSString*)[prefs valueForKey:key];
    return data;
}
+(BOOL)boolPropertyDataWithKey:(NSString *)key{
    
    NSString *path = [self getPath];
    
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    NSNumber * boolNumber = [prefs valueForKey:key];
    BOOL data = [boolNumber boolValue];
    return data;
}

+(NSString *)getPath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:propertiesKey];
    return path;
}

+(void)deActivateButton:(NSButton *)button{
    button.alphaValue = 0.5;
    [button setEnabled:NO];
}

+(void)activateButton:(NSButton *)button{
    button.alphaValue = 1.0;
    [button setEnabled:YES];
}


@end
