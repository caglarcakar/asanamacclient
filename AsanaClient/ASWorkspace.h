//
//  ASWorkspace.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ASWorkspace : NSObject
@property(copy)NSString* ID;
@property(copy)NSString* name;
@property NSInteger badgeValue;
@property BOOL isHeader;
@end
