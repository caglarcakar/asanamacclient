//
//  ASUserManager.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASUser.h"
#import <CoreData/CoreData.h>

@interface ASUserManager : NSObject
+ (void)saveUser:(ASUser *)obj;
+ (ASUser *)loadUser;
@end
