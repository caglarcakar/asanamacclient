//
//  ASTaskCellViewController.h
//  AsanaClient
//
//  Created by caglar cakar on 8/3/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ASTaskCellViewController : NSTableCellView

@property (strong) IBOutlet NSButton *completedCheck;

@end
