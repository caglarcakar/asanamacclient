//
//  ASWorkspaceListController.h
//  AsanaClient
//
//  Created by caglar cakar on 8/2/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol workspaceDelegate<NSObject>
@optional
-(void)workspaceSelected:(ASWorkspace *)aWorkspace;
@end
@interface ASWorkspaceListController : NSObject<NSOutlineViewDataSource,NSOutlineViewDelegate>
-(id)initWithArray:(NSMutableArray *)anArray;
@property(copy)NSMutableArray *workspacesArray;
@property(nonatomic,strong)id<workspaceDelegate>delegate;
@end
