//
//  ASLoginViewController.h
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol loginDelegate <NSObject>

-(void)LoginSuccess;

@end

@interface ASLoginViewController : NSViewController
@property (strong, nonatomic) id<loginDelegate>delegate;

@property (strong) IBOutlet NSTextField *apikeyInput;
@property (strong) IBOutlet NSButton *loginButton;

@end
