//
//  DijitalGarajRequestObject.h
//  Dijital Garaj
//
//  Created by caglar cakar on 3/29/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DijitalGarajRequestObject : NSObject


@property(nonatomic,copy)NSString *requestCustomUrl;
@property(nonatomic)NSInteger tag;
@property(nonatomic,strong)NSMutableDictionary *parameters;
@property(nonatomic,copy)NSString *api;
@property(nonatomic,copy)NSString *method;

@property(nonatomic)BOOL requestUseCustomURL;
@property(nonatomic)BOOL requestShowProgress;


-(id)initWithApi:(NSString *)anApi andParameters:(NSMutableDictionary *)theParameters andTag:(NSInteger )aTag requsetMethod:(NSString *)aMethod showProgress:(BOOL)progress;
-(void)startRequestWithSuccess:(void (^)(NSInteger tag, NSString * message, id responseObject))successfull failure:(void (^)(NSInteger tag, NSString * message))failure;



@end
