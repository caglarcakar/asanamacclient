//
//  defines.h
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#ifndef AsanaClient_defines_h
#define AsanaClient_defines_h

#define keyforAPIKEY                            @"asanaAPIKEY"
#define kASANA_API_URL                          @"https://app.asana.com/api/1.0"
#endif
