//
//  ASMainViewController.h
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ASWorkspaceListController.h"
#import "ASProjectListController.h"
#import "ASTaskListController.h"

@interface ASMainViewController : NSViewController<workspaceDelegate,projectDelegate,taskDelegate>
@property(nonatomic)NSMutableArray *workspaces;
@property(nonatomic)NSMutableArray *projects;
@property(nonatomic)NSMutableArray *tasks;

@property(nonatomic)ASWorkspace *selectedWorkSpace;
@property(nonatomic)ASProject *selectedProject;
@property(nonatomic)ASTask *selectedTask;


@property (strong) IBOutlet NSOutlineView *workspaceTable;
@property (strong) IBOutlet NSTableView *projectTable;
@property (strong) IBOutlet NSTableView *TaskTable;

@property (strong) IBOutlet ASWorkspaceListController *workspaceController;
@property (strong) IBOutlet ASProjectListController *projectController;
@property (strong) IBOutlet ASTaskListController *taskController;

@property (strong) IBOutlet NSButton *completeCheck;
@property (strong) IBOutlet NSTextField *taskName;
@property (strong) IBOutlet NSTextField *taskDescription;

@end
