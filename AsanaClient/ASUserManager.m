//
//  ASUserManager.m
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASUserManager.h"

#define userKey                    @"user"
#define UserPlistName              @"userData.plist"

@implementation ASUserManager

+(void)saveUserData:(NSData *)data withKey:(NSString *)key{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:UserPlistName];
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:path];
    
    
    if (fileExists) {
        //pulls data from existing file saves to oldvalues array
        NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        [prefs setValue: data forKey:key];
        //NSLog(@"document count = %i",[prefs count]);
        [prefs writeToFile: path atomically: YES];
    }
    else {
        //create new plist if non-existing
        NSMutableDictionary* prefs = [NSMutableDictionary dictionary];
        [prefs setValue: data forKey:key];
        //NSLog(@"document count = %i",[prefs count]);
        [prefs writeToFile: path atomically: YES];
        
    }
}

+(void)changeUserData:(BOOL)data withKey:(NSString *)key{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:UserPlistName];
    //NSLog(@"%@",path);
    
    
    //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"userData" ofType:@"plist"];
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    [prefs setObject:[NSNumber numberWithBool:data] forKey:key];
    [prefs writeToFile: path atomically: YES];
}

+(NSData *)UserDataWithKey:(NSString *)key{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:UserPlistName];
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    NSData* data = (NSData*)[prefs valueForKey:key];
    return data;
}
+(BOOL)UserBoolDataWithKey:(NSString *)key{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:UserPlistName];
    //NSLog(@"%@",path);
    //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"userData" ofType:@"plist"];
    NSMutableDictionary* prefs = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    
    NSNumber * boolNumber = [prefs valueForKey:key];
    BOOL data = [boolNumber boolValue];
    return data;
}


+ (void)saveUser:(ASUser *)obj {
    NSData *userObject = [NSKeyedArchiver archivedDataWithRootObject:obj];
    [self saveUserData:userObject withKey:userKey];
}

+ (ASUser *)loadUser{
    ASUser *obj = (ASUser *)[NSKeyedUnarchiver unarchiveObjectWithData: [self UserDataWithKey:userKey]];
    return obj;
}


@end
