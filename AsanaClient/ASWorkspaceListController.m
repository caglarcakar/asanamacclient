//
//  ASWorkspaceListController.m
//  AsanaClient
//
//  Created by caglar cakar on 8/2/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASWorkspaceListController.h"
#import "ASWorkspace.h"
@implementation ASWorkspaceListController
-(id)initWithArray:(NSMutableArray *)anArray{
    
       self = [super init];
    if(self){
        self.workspacesArray = [[NSMutableArray alloc] initWithArray:anArray];
    }
    return self;
}


-(BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item{
    
    return NO;
}

-(NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item{
    if(!item){
        return [self.workspacesArray count];
    }
    
    return 0;
}


-(id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item{
    if(!item){
    ASWorkspace *aWorkspace = [ASWorkspace new];
    aWorkspace = [self.workspacesArray objectAtIndex:index];
    
    return [self.workspacesArray objectAtIndex:index];
    }

    return @"NO CHILD";
}

-(BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item{
    if([item isHeader]){
        return NO;
    }
return YES;
}


-(NSView *)outlineView:(NSOutlineView *)ov viewForTableColumn:(NSTableColumn *)tableColumn item:(ASWorkspace *)item {
  NSTableCellView *result;
    
    if(item.isHeader){
        result = [ov makeViewWithIdentifier:@"HeaderCell" owner:self];
    }
    else{
    result = [ov makeViewWithIdentifier:@"DataCell" owner:self];
    }
  [[result textField] setStringValue:[item name]];
  return result;
}

-(void)outlineViewSelectionDidChange:(NSNotification *)notification{
    NSLog(@"selected %li",(long)[notification.object selectedRow]);
    
    if ([self.delegate respondsToSelector:@selector(workspaceSelected:)]) {
    [self.delegate workspaceSelected:[self.workspacesArray objectAtIndex:[notification.object selectedRow]]];
    }
    
}
@end
