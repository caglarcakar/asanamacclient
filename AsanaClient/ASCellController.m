//
//  ASCellController.m
//  AsanaClient
//
//  Created by caglar cakar on 8/4/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASCellController.h"

@interface ASCellController ()

@end

@implementation ASCellController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
