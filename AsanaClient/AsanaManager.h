//
//  AsanaManager.h
//  AsanaClient
//
//  Created by caglar cakar on 8/1/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AsanaManager : NSObject

+(void)login:(void (^)(BOOL requestSuccess, NSString *message, ASUser *user))successfull failure:(void (^)(NSString *message))failure;
+(void)getWorkSpaces:(void (^)(BOOL requestSuccess, NSString * message, NSMutableArray *remoteWorkspaces))successfull failure:(void (^)(NSString *message))failure;
+(void)getProjects:(NSString *)workspaceID success:(void (^)(BOOL requestSuccess, NSString *message, NSMutableArray *remoteprojects))successfull failure:(void (^)(NSString *message))failure;
+(void)getTasks:(NSString *)taskID success:(void (^)(BOOL requestSuccess, NSString *message, NSMutableArray *remotetasks))successfull failure:(void (^)(NSString *message))failure;

+(void)getTaskDetail:(NSString *)taskID success:(void(^)(BOOL requestSuccess, NSString * message, ASTask *remoteSelectedTask))successfull failure:(void (^)(NSString *message))failure;

+(void)saveTaskDetail:(ASTask *)aTask success:(void(^)(BOOL requestSuccess,NSString *message))successfull failure:(void(^)(NSString *message))failure;
@end
