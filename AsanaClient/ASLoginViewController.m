//
//  ASLoginViewController.m
//  AsanaClient
//
//  Created by caglar cakar on 7/30/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import "ASLoginViewController.h"

@interface ASLoginViewController ()

@end

@implementation ASLoginViewController{
    DijitalGarajRequestObject *loginRequest;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
- (IBAction)loginAction:(id)sender {
    
    [ASUtility deActivateButton:self.loginButton];
    
    if ([self.apikeyInput stringValue].length>0) {
        
        [ASUtility savePropertyData:[self.apikeyInput stringValue] withKey:keyforAPIKEY];
        
        [AsanaManager login:^(BOOL requestSuccess, NSString *message, ASUser *user) {
            if(requestSuccess){
                [ASUserManager saveUser:user];
                [self.delegate LoginSuccess];
            }
            else{
                [ASUtility showAlertWithText:message withDetailText:@"" inWindow:[self.view window]];
                [ASUtility activateButton:self.loginButton];
            }
        } failure:^(NSString *message) {
            [ASUtility showAlertWithText:message withDetailText:@"" inWindow:[self.view window]];
             [ASUtility activateButton:self.loginButton];
        }];
    }
    else{
        [ASUtility showAlertWithText:@"Please Enter Your Key" withDetailText:@"" inWindow:[self.view window]];
        [ASUtility activateButton:self.loginButton];
    }
    
    
}

/*
 login data
 
 data =     {
 email = "caglar@dijitalgaraj.com";
 id = 6099284848027;
 name = "Caglar Cakar";
 photo =         {
 "image_128x128" = "https://s3.amazonaws.com/profile_photos/6099284848027.koPtZ1elHRBgfzXottLi_huge.jpeg";
 "image_21x21" = "https://s3.amazonaws.com/profile_photos/6099284848027.koPtZ1elHRBgfzXottLi_21x21.png";
 "image_27x27" = "https://s3.amazonaws.com/profile_photos/6099284848027.koPtZ1elHRBgfzXottLi_27x27.png";
 "image_36x36" = "https://s3.amazonaws.com/profile_photos/6099284848027.koPtZ1elHRBgfzXottLi_36x36.png";
 "image_60x60" = "https://s3.amazonaws.com/profile_photos/6099284848027.koPtZ1elHRBgfzXottLi_60x60.png";
 };
 workspaces =         (
 {
 id = 6099293616334;
 name = "dijitalgaraj.com";
 },
 {
 id = 498346170860;
 name = "Personal Projects";
 },
 {
 id = 852167829747;
 name = "Dijital Garaj";
 },
 {
 id = 2502444715279;
 name = "My Projects";
 }
 );
 };

 
 
 */

@end
