//
//  ASProjectListController.h
//  AsanaClient
//
//  Created by caglar cakar on 8/3/13.
//  Copyright (c) 2013 Dijital Garaj. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol projectDelegate<NSObject>
@optional
-(void)projectSelected:(ASProject *)aProject;
@end
@interface ASProjectListController : NSObject<NSTableViewDataSource,NSTableViewDelegate>
-(id)initWithArray:(NSMutableArray *)anArray;
@property(copy)NSMutableArray *projectsArray;
@property(nonatomic,strong)id<projectDelegate>delegate;
@end
